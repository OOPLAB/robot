/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.robotproject;

/**
 *
 * @author User
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = ' ';
    
    public  Robot(int x,int y,int bx,int by,int N){
        this.x=x;
        this.y=y;
        this.bx=bx;
        this.by=by;
        this.N=N;
    }
    public boolean inMap(int x,int y){
         if(x>=N||x<0||y-1>=N||y-1<0){
            return false;
        }
         return true;
    }

       //N, S, E, W
    //y-1,y+1,x+1,x-1
    public boolean walk(char direction){
        switch(direction){
            
            case 'N':
                if(!inMap(x,y-1)){
                    printUnableMove();
                    return false;
                }
                y=y-1;
                break;
            case 'S':
                if(!inMap(x,y+1)){
                    printUnableMove();
                    return false;
                }
                y=y+1;
                break;
            case 'E':
                if(!inMap(x+1,y)){
                    printUnableMove();
                    return false;
                }
                x=x+1;
                break;
            case 'W':
                if(!inMap(x-1,y)){
                    printUnableMove();
                    return false;
                }
                x=x-1;
                break;
        }
        
        lastDirection = direction;
        if(isBomb()){
            printBomb();
        }
        return true;
    }
    public boolean walk(char direction,int step){
        for(int i=0;i<step;i++){
            if (!walk(direction)){
                return false;
            }
            
        }
        return true;
    }
    public boolean walk(){
       return  walk(lastDirection);
    }  
    public boolean walkNStep(int step){
       return  walk(lastDirection,step);
    }  
    public String toString(){
        return "Robot ("+ this.x+", "+this.y+") ";
        
    }
    public void printUnableMove(){
        System.out.println("I can't move!!!");
    }
    public void printBomb(){
        System.out.println("Bomb found!!!");
    }
    public boolean isBomb(){
       if(x==bx&&y==by){
           return true;
       }
       return false;
    }
}
